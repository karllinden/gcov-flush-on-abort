# Linker preload for dumping GCOV data

## Introduction

The `gcov-dump-on-abort.so` shared module can be used with `LD_PRELOAD` to dump
GCOV data, using `__gcov_dump()`, when `abort()` is called.
By default `__gcov_dump()` is only called at normal termination, so this module
is useful to get coverage data also for code paths that lead to abnormal
termination.

## Installation

The `gcov-dump-on-abort.so` module together with its accompanying pkg-config
file can be installed with:

```bash
meson build --buildtype=minsize
ninja -C build
sudo ninja -C build install
```

## Usage

To make the dynamically linked executable `myexe` dump GCOV data on `abort()`,
run this command, assuming that the module has been installed to
`/usr/local/lib`:

```bash
LD_PRELOAD=/usr/local/lib/gcov-dump-on-abort.so myexe
```

Meson projects can use the module in tests, by extracting the `ld_preload`
variable defined in the pkg-config file and passing it to the `test(...)`
function.
For example:

```meson
env = environment()

gcov_dump_dep = dependency('gcov-dump-on-abort', required: false)
if gcov_dump_dep.found()
  env.append('LD_PRELOAD', gcov_dump_dep.get_pkgconfig_variable('ld_preload'))
endif

test(..., env: env, ...)
```
